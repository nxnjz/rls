#!/usr/bin/bash

ifname=ens18
user=root
cidr=16
gateway="192.168.1.1"
dns="192.168.1.1"

for host in $@
do
	read -p "static ip for $host ? " newip
	tmpfile=`mktemp`
	echo "writing to $tmpfile"
	{
	echo "source /etc/network/interfaces.d/* "
	echo "auto lo"
	echo "iface lo inet loopback"
	echo "allow-hotplug $ifname"
	echo "iface $ifname inet static"
	echo "address ${newip}/${cidr}"
	echo "gateway $gateway"
	echo "dns-nameservers $dns"
        } >> $tmpfile
        scp $tmpfile ${user}@${host}:/etc/network/interfaces && echo "interfaces file installed on $host"
	ssh ${user}@${host} "chmod 0644 /etc/network/interfaces" && echo "interfaces file chmoded 0644"
	echo "rebooting host in 3s, Ctrl+C to cancel"
	sleep 3 && ssh ${user}@${host} "reboot || shutdown -r now" && echo "rebooting $host"
	rm $tmpfile && echo "removed $tmpfile"

done
