#!/usr/bin/bash

user=root

for host in $@
do
	oldname=`ssh ${user}@${host} "hostname"`
	read -p "new hostname for $host $oldname? " newname
	ssh ${user}@${host} "sed -i \"s/${oldname}/${newname}/g\" /etc/hosts"
	ssh ${user}@${host} "sed -i \"s/${oldname}/${newname}/g\" /etc/hostname"
	echo "rebooting host in 3s, Ctrl+C to cancel"
	sleep 3 && ssh ${user}@${host} "reboot || shutdown -r now" && echo "rebooting $host"

done
