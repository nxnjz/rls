# USAGE: source quick_PS1.sh username hostname
# for screenshots/demos
export PS1="\[\033[38;5;2m\]${1}\[$(tput sgr0)\]\[\033[38;5;15m\]@\[$(tput sgr0)\]\[\033[38;5;10m\]${2}\[$(tput sgr0)\]\[\033[38;5;15m\]:\w\\$ \[$(tput sgr0)\]"
